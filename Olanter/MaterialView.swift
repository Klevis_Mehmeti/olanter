//
//  MaterialView.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 17/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit

class MaterialView: UIView {

    override func awakeFromNib() {
        layer.cornerRadius = 3.0
        layer.shadowColor = UIColor(red: ShadowColor, green: ShadowColor, blue: ShadowColor, alpha: 0.5).cgColor
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    }

}
