//
//  SignUpVc.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 21/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit
import Firebase

class SignUpVc: UIViewController ,UITextFieldDelegate{
    @IBOutlet weak var emailL: UITextField!
    @IBOutlet weak var passL: UITextField!
    @IBOutlet weak var passTryL: UITextField!
    @IBOutlet weak var username :UITextField!
    @IBOutlet weak var imgScreen: MaterialImage!
    
    var data = NSData()
    let storage = FIRStorage.storage()
    var ref: FIRDatabaseReference!
    let metaData = FIRStorageMetadata()
    let standartImgUrl = "https://firebasestorage.googleapis.com/v0/b/olanter-9b6f1.appspot.com/o/User%20Images%2Fnoun_697826_cc%20copy.png?alt=media&token=0d4fb092-c876-48c9-8c3a-f931077b316a"
    override func viewDidLoad() {
        super.viewDidLoad()
        
      emailL.delegate = self
      passTryL.delegate = self
        username.delegate = self
        passTryL.delegate = self
        ref = FIRDatabase.database().reference()        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func SignUpTry(_ sender: Any) {
        
        if let email = emailL.text, email != "" ,let pass = passL.text ,pass != "", let passTry = passTryL.text, passTry != "" , username.text != ""
        {
            if pass == passTry
            {
                
                
                FIRAuth.auth()?.createUser(withEmail: email, password: pass, completion: { (user, error) in
                   var usrn = (self.username.text)!
                    self.ref.child("users").child(user!.uid).setValue(["username": "\(usrn)","provider": "email" , "likes" : "","profilUrl":self.standartImgUrl ])
                    
                    
                    if error != nil
                    {
                        if let errCode = FIRAuthErrorCode(rawValue: error!._code) {
                            switch errCode.rawValue
                            {
                            case emailGabim : self.showErrorFunc(title: "Wrong email", message: "Write a valuable email ")
                            case emailExist : self.showErrorFunc(title: "Wrong email", message: "Email already exists")
                            default : self.showErrorFunc(title: " Contact to us", message: "For the problems")
                            }
                           
                        }
                    
                    }
                    else {
                     self.performSegue(withIdentifier: "ViewController", sender: nil)
                    
                    }
                })
                
            }
            else
            {
                showErrorFunc(title: "Passwords", message: "Aren't identical")
                
            }
            
            
        }
            
        else
        {
            showErrorFunc(title: "Fill the boxes", message: "Please fill the email and password!")
        }
        
    }
    
    
    
    func showErrorFunc(title : String , message : String)  {
        let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return false to ignore.
    {
        textField.resignFirstResponder()
        return true
    }
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
