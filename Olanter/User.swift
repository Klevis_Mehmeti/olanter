//
//  User.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 18/02/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import Foundation

class User {
    private var _username = ""
    private var _profilUrl = ""
    private var _provider = ""
    
    init(username : String, provider : String , profilUrl : String) {
        self._username = username
        self._profilUrl = profilUrl
        self._provider = provider
    }
    
    
    var username : String {
        return _username
    
    }

    var profilUrl : String {
        return _profilUrl
    }
    
    var provider : String {
    
        return _provider
    }


}
