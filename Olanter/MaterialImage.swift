//
//  MaterialImage.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 26/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit

class MaterialImage: UIImageView {
    override func awakeFromNib() {
        self.layer.cornerRadius = frame.height/2
        self.layer.shadowColor = UIColor(red: ShadowColor, green: ShadowColor, blue: ShadowColor, alpha: 0.5).cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        clipsToBounds = true
        layer.masksToBounds = false
    }
  
}
