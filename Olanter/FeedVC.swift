//
//  FeedVC.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 23/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import AlamofireImage
import Alamofire
import FirebaseStorage
import Kingfisher


class FeedVC: UIViewController ,UITableViewDelegate , UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate , UITextFieldDelegate{
    @IBOutlet weak var drawerMenu: UIView!
    @IBOutlet weak var dimMenu: UIView!
    @IBOutlet weak var drawerRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var textArea: MaterialTextField!
    var posts = [Post]()
    var userInfo : User?
    var imgPost : UIImage?
    var imgBool = false
    var imgTemp : UIImage?
    let picker = UIImagePickerController()
    var data = NSData()
    let metaData = FIRStorageMetadata()
    let storage = FIRStorage.storage()
    var imageCache : UIImageView?
    var ref: FIRDatabaseReference!
    var refLikes: FIRDatabaseReference!
    var refreshControl: UIRefreshControl!
    let user = FIRAuth.auth()?.currentUser
    
    
    override func viewWillAppear(_ animated: Bool) {
        gettingUserInfo()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textArea.delegate = self
        drawerRightConstraint.constant = -700
        dimMenu.alpha = 0
        refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        
        let imgRef = storage.reference(forURL: "gs://olanter-9b6f1.appspot.com/Images Post/")
        picker.delegate = self
        picker.allowsEditing = true
        refLikes = FIRDatabase.database().reference().child("users").child(user!.uid).child("likes")
        ref = FIRDatabase.database().reference()
        
        GetPosts()
        
        tableView.delegate = self
        tableView.dataSource = self
        gettingUserInfo()
        
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        let post =  posts[indexPath.row]
        cell.configureCell(post: post )
        
        return cell
        
    }
    
 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if posts[indexPath.row].imageUrl == nil {
            return 150
        }
        else {
            return 364
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgPost = image
            self.imgCamera.image = image
            self.imgBool = true
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickCamera(_ sender: Any) {
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func clickPost(_ sender: Any) {
        
        if (imgBool){
            
            if textArea.text != "" && textArea.text != nil {
                
                data = UIImageJPEGRepresentation(imgCamera.image!, 0.09)! as NSData
                let imageUniqueName = UUID().uuidString
                storage.reference(forURL: "gs://olanter-9b6f1.appspot.com/Images Post/\(imageUniqueName)").put(data as Data, metadata: metaData, completion: { (metadata, error) in
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }
                    else {
                        var childCode = self.ref.child("post").childByAutoId()
                        let date = Date()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd.MM.yyyy"
                        let result = formatter.string(from: date)
                        let calendar = NSCalendar.current
                        let hour = calendar.component(.hour, from: date)
                        let minutes = calendar.component(.minute, from: date)
                        
                        
                        
                        let downloadURL = metadata!.downloadURL()!
                        childCode.setValue(["description":self.textArea.text,"imageUrl": "\(downloadURL)","likes" : 0 ,"timestamp" : self.getTimeStamp(),"username" : (self.userInfo?.username)!,"userUrl" : (self.userInfo?.profilUrl)!,"date" :"\(result)@\(hour):\(minutes)"])
                        
//                        self.ref.child("post").childByAutoId().setValue(["description":self.textArea.text,"imageUrl": "\(downloadURL)","likes" : 0 ,"timestamp" : self.getTimeStamp(),"username" : (self.userInfo?.username)!,"userUrl" : (self.userInfo?.profilUrl)!,"date" :"\(result)@\(hour):\(minutes)"])
                        self.textArea.text = ""
                        self.imgBool = false
                        self.imgCamera.image = UIImage(named: "camera")
                        self.tableView.reloadData()
                        self.ref.child("users").child((self.user?.uid)!).child("posts").childByAutoId().setValue("\(childCode.key)")
                    }
                })
            }
            
        }
        else
        {   var childCode = self.ref.child("post").childByAutoId()
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            let result = formatter.string(from: date)
            let calendar = NSCalendar.current
            let hour = calendar.component(.hour, from: date)
            let minutes = calendar.component(.minute, from: date)
            childCode.setValue(["description":self.textArea.text,"likes" : 0 ,"timestamp" : self.getTimeStamp(),"username" : (self.userInfo?.username)!,"userUrl" : (self.userInfo?.profilUrl)!,"date" :"\(result)@\(hour):\(minutes)"])
//            self.ref.child("post").childByAutoId().setValue(["description":self.textArea.text,"likes" : 0 ,"timestamp" : self.getTimeStamp() as Double,"username" : (self.userInfo?.username)!,"userUrl" : (self.userInfo?.profilUrl)!,"date" :"\(result)@\(hour):\(minutes)"])
            self.textArea.text = ""
            self.tableView.reloadData()
            self.ref.child("users").child((self.user?.uid)!).child("posts").childByAutoId().setValue("\(childCode.key)")
        }
        
        
        
        
    }
    
    func refresh(sender:AnyObject) {
        
        posts.removeAll()
        GetPosts()
        tableView.reloadData()
        refreshControl.endRefreshing()
        
    }
    
    func GetPosts() {
        var  refHandle = ref.child("post").observe(FIRDataEventType.value, with: { (snapshot) in
            self.posts = []
            print(snapshot)
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            for poste in postDict {
                
                
                if let post = poste.value as? Dictionary<String,AnyObject>{
                    let key = poste.key
                    
                    let postObject = Post(postKey: key, dictionary: post)
                    // kujdes cast qe i eshte bere member te klases post ,, kontrollo nqs ke prob ne te ardhmen
                    self.posts.append(postObject)
                }
            }
            
            
            print(postDict)
            self.tableView.reloadData()
            self.posts.sort {
                $0.timestamp > $1.timestamp
            }
            
            
        })
        
        
        
        
    }
 
    
 @IBAction func loggingOut(_ sender: UIButton) {
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "firstVc") as UIViewController
            present(secondViewController, animated: true, completion: nil)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
  
 func getTimeStamp()->Double{
        print("ketu fillon")
        let timestamp = (Date().timeIntervalSince1970)
        print(timestamp)
//        let reversedTimestamp = -1.0 * timestamp
//        print(reversedTimestamp)
        return timestamp
    }
    
    @IBAction func drawerClick(_ sender: UIBarButtonItem) {
        if drawerRightConstraint.constant == -16 {
        
        UIView.animate(withDuration: 0.4, animations: {
            //e mbyllim
            self.drawerRightConstraint.constant = -700
            self.dimMenu.alpha = 0
            self.drawerMenu.superview?.layoutIfNeeded()
        })
        
        }
        else {
            
            // e hapim
            UIView.animate(withDuration: 0.4, animations: {
                self.dimMenu.alpha = 0.7
                self.drawerRightConstraint.constant = -16
                self.drawerMenu.superview?.layoutIfNeeded()
            })
        
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditAccount" {
            if let editAccount = segue.destination as? Edit_Account {
                if let userValues = self.userInfo {
                
                    editAccount.userInfo = userValues
                }
            
            }
            
            UIView.animate(withDuration: 0.4, animations: {
                //e mbyllim
                self.drawerRightConstraint.constant = -700
                self.dimMenu.alpha = 0
                self.drawerMenu.superview?.layoutIfNeeded()
            })
        
        }
    }
    
    func gettingUserInfo(){
    let userID = FIRAuth.auth()?.currentUser?.uid
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
           
            // Get user value
            let value = snapshot.value as? NSDictionary
            print(value)
            let username = value?["username"] as? String ?? ""
            let provider = value?["provider"] as? String ?? ""
            let profilUrl = value?["profilUrl"] as? String ?? ""
            self.userInfo = User.init(username: username, provider: provider, profilUrl: profilUrl)
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
    
    }
    @IBAction func dragInside(_ sender: UIButton) {
        print("u futa")
        
        // e hapim
        UIView.animate(withDuration: 0.4, animations: {
            self.dimMenu.alpha = 0.7
            self.drawerRightConstraint.constant = -16
            self.drawerMenu.superview?.layoutIfNeeded()

        })
        
    }
    @IBAction func dragOutside(_ sender: UIButton) {
        if drawerRightConstraint.constant == -16 {
            
            UIView.animate(withDuration: 0.4, animations: {
                //e mbyllim
                self.drawerRightConstraint.constant = -700
                self.dimMenu.alpha = 0
                self.drawerMenu.superview?.layoutIfNeeded()
            })
            
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


