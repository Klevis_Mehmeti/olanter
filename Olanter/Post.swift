//
//  Post.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 29/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import Foundation
import UIKit
import Firebase


class Post {

private var _descriptionPost : String!
private var _likes : Int!
private var _imageURL : String?
private var _username : String!
private var _postKey : String!
var userName : String?
var profilUrl : String?
var image : UIImage?
var refLikes: FIRDatabaseReference!
var timestamp : Double = 0.0
    var date : String?

    
    var descriptionPost : String! {
    
        return _descriptionPost
    
    }
    
    var likes : Int {
    
    return _likes
    
    }
    
    
    var imageUrl : String? {
    
    return _imageURL
    }
    
    
    var username : String! {
    return _username
    
    }
    
    var postKey : String! {
    return _postKey
    
    }
   
    
    
    
    init(description : String, imgURL : String? , username : String) {
        
        _descriptionPost = descriptionPost
        _imageURL = imgURL
        _username = username
    }
    
    init(postKey : String , dictionary : Dictionary<String ,AnyObject>) {
        _postKey = postKey
        
        if let like = dictionary["likes"]{
            self._likes = like as! Int
           
        }
        
        if let desc = dictionary["description"] {
            self._descriptionPost = desc as! String
        
        }
        
        if let imgUrl = dictionary["imageUrl"] {
        
            self._imageURL = imgUrl as? String
        }
        
        if let time = dictionary["timestamp"] {
            self.timestamp = time as! Double
        
        }
        
        if let username = dictionary["username"]{
        
        self.userName = username as! String
        }
        
        if let profilUrl = dictionary["userUrl"] {
        
        self.profilUrl = profilUrl as! String
        }
        
        if let date = dictionary["date"]{
        self.date = date as! String
        }
      refLikes = FIRDatabase.database().reference().child("post").child(self._postKey).child("likes")
    }
    
    

    
    func adjustLikes(addlike:Bool){
        if addlike {
        _likes = _likes + 1
        
        }
        else {
            _likes = _likes - 1
        }
    refLikes.setValue(_likes)
    }
}
