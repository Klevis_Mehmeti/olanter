//
//  Edit Account.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 17/02/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import AlamofireImage
import Alamofire
import FirebaseStorage
import Kingfisher

class Edit_Account: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate ,UITextFieldDelegate{
 
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var retypePasswordTextField: MaterialTextField!
    @IBOutlet weak var passwordTextfield: MaterialTextField!
    
    @IBOutlet weak var usernameTextField: MaterialTextField!
    var ref: FIRDatabaseReference!
    let storage = FIRStorage.storage()
    let user = FIRAuth.auth()?.currentUser
    let standartImgUrl = "https://firebasestorage.googleapis.com/v0/b/olanter-9b6f1.appspot.com/o/User%20Images%2Fnoun_697826_cc%20copy.png?alt=media&token=0d4fb092-c876-48c9-8c3a-f931077b316a"
    var userInfo : User?
    var url : URL?
    let picker = UIImagePickerController()
    var data = NSData()
    let metaData = FIRStorageMetadata()
    var imgBool = false
    var username : String?
    var imgUrl : String?
    var idPosts = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        retypePasswordTextField.delegate = self
        passwordTextfield.delegate = self
        usernameTextField.delegate = self
        ref = FIRDatabase.database().reference().child("users").child(user!.uid)
        let imgRef = storage.reference(forURL: "gs://olanter-9b6f1.appspot.com/Images Profil/")
        picker.delegate = self
        picker.allowsEditing = true
        self.navigationItem.title = "Edit Account"
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.default
        nav?.tintColor = UIColor.white
        username = userInfo?.username
        usernameTextField.text = username
        imgUrl = userInfo?.profilUrl
        getUserPostUrl()
        if imgUrl != nil && imgUrl != "" && imgUrl != standartImgUrl {
        url = URL(string: imgUrl!)
        userImg.kf.setImage(with: url)
        userImg.layer.cornerRadius = userImg.bounds.height / 2
        userImg.clipsToBounds = true
        }
        
       
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userImg.image = image
            imgBool = true
            
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func imgButton(_ sender: UIButton) {
        present(picker, animated: true, completion: nil)
    }
    @IBAction func saveChanges(_ sender: UIButton) {
        if usernameTextField.text != username && usernameTextField.text != "" && usernameTextField != nil {

          ref.updateChildValues(["username" : usernameTextField.text!])
        }
        if passwordTextfield.text != "" {
            if retypePasswordTextField.text != "" {
                if passwordTextfield.text == retypePasswordTextField.text{
                    FIRAuth.auth()?.currentUser?.updatePassword(passwordTextfield.text!, completion: { (error) in
                        if error != nil {
                        self.showErrorFunc(title: "Failed", message: "Password did not change. Log in again before retrying this request")
                        print(error)
                        }
                        else {
                        self.passwordTextfield.text = ""
                        self.retypePasswordTextField.text = ""
                        }
                    })
                    
                }
                else{
                    showErrorFunc(title: "Passwords", message: "Password fields are not equal")
                    
                }
                
            
            }
            else {
            
            showErrorFunc(title: "Password", message: "Fill the password box")
            }
        
        }
        if imgBool {
        
            data = UIImageJPEGRepresentation(userImg.image!, 0.09)! as NSData
            let imageUniqueName = UUID().uuidString
            storage.reference(forURL: "gs://olanter-9b6f1.appspot.com/User Images/\(imageUniqueName).jpg").put(data as Data, metadata: metaData, completion: { (metadata, error) in
                if let error = error {
                    self.showErrorFunc(title: "Error", message: "Image did not upload")
                    print(error.localizedDescription)
                    return
                }
                else {
                    let downloadURL = metadata!.downloadURL()!
                    
                    self.ref.updateChildValues(["profilUrl" : "\(downloadURL)" ], withCompletionBlock: { (error, FIRDatabaseReference) in
                        if error != nil {
                        self.showErrorFunc(title: "Error", message: "Image did not upload")
                        }
                        else {
                         self.showErrorFunc(title: "Success", message: "Image did upload successfuly")
                            
                        }
                    })

                }
            })
        
        
    }
    
    
        }
    

    func showErrorFunc(title : String , message : String)  {
        let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func getUserPostUrl(){
        let userID = FIRAuth.auth()?.currentUser?.uid
        ref.child("users").child(userID!).child("posts").observeSingleEvent(of: .value, with: { (snapshot) in
            
            // Get user value
            let value = snapshot.value as? NSDictionary
            print("22\(value?["-KdSa9hBZoDLq6qLEYVR"])")

        }) { (error) in
            print(error.localizedDescription)
        }
    
    }

}
