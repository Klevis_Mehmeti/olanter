//
//  PostCellTableViewCell.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 23/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

class PostCell: UITableViewCell {
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var imageProfil : UIImageView!
    @IBOutlet weak var imageHeadLine : UIImageView!
    @IBOutlet weak var heartCell : UIImageView!
    @IBOutlet weak var textArenaCell: UITextView!
    @IBOutlet weak var likesNumberCell: UILabel!
    @IBOutlet weak var usernameCell: UILabel!
    var ref: FIRDatabaseReference!
    var refLikes: FIRDatabaseReference!
    var post : Post!
    var refUser : FIRDatabaseReference!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let user = FIRAuth.auth()?.currentUser
        refLikes = FIRDatabase.database().reference().child("users").child(user!.uid).child("likes")
        ref = FIRDatabase.database().reference()
        refUser = FIRDatabase.database().reference().child("users").child(user!.uid).child("likes")
        // qe te aktivizohet klikimi ose tap i nje label ose nje img ose butoni duhet te deklarohet tek awakefromNib (aka funksion i cili aktivizohet pasi hapet te gjitha view e class)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(likeTap(sender:)))
        tap1.numberOfTapsRequired = 1
        heartCell.addGestureRecognizer(tap1)
        heartCell.isUserInteractionEnabled = true
    }
    override func draw(_ rect: CGRect) {
        imageProfil.layer.cornerRadius = imageProfil.frame.size.width / 2
        imageProfil.clipsToBounds = true
    }

    
    
    func configureCell(post : Post){
        self.post = post
        print("klevis\(post.timestamp)")
        textArenaCell.text = post.descriptionPost
        likesNumberCell.text = "\(post.likes)"
        if post.profilUrl != nil {
        var url = URL(string: post.profilUrl!)
        imageProfil.kf.setImage(with: url)
        }
        usernameCell.text = post.userName
        self.time.text = post.date
        
        refLikes.child(post.postKey).observeSingleEvent(of: .value, with: { (snapshot) in
            if let doesNotExist = snapshot.value as? NSNull {
                self.heartCell.image = UIImage(named: "unfilled heart")
            }
            else {
                self.heartCell.image = UIImage(named: "filled heart")
            }
        })
        if post.imageUrl != nil  {
            let url = URL(string: post.imageUrl!)
            imageHeadLine.kf.setImage(with: url)
        
        }
        
        
    }
    
    func likeTap(sender:UITapGestureRecognizer) {
        
        refLikes.child(post.postKey).observeSingleEvent(of: .value, with: { (snapshot) in
            if let doesNotExist = snapshot.value as? NSNull {
                
                self.heartCell.image = UIImage(named: "filled heart")
                self.post.adjustLikes(addlike: true)
                self.refLikes.child(self.post.postKey).setValue(true)
                
            }
            else {
                self.heartCell.image = UIImage(named: "unfilled heart")
                self.post.adjustLikes(addlike: false)
                self.refLikes.child(self.post.postKey).setValue(false)
                self.refLikes.child(self.post.postKey).removeValue()
            }
        })
    }
    
}
