//
//  ViewController.swift
//  Olanter
//
//  Created by Klevis Mehmeti on 16/01/17.
//  Copyright © 2017 Klevis Mehmeti. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import SwiftyJSON


class ViewController: UIViewController ,UITextFieldDelegate {
    
    
    @IBOutlet weak var email : UITextField!
    @IBOutlet weak var password : UITextField!
    var ref: FIRDatabaseReference!
    override func viewDidLoad() {
       
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        email.delegate = self
        password.delegate = self
        if let user = FIRAuth.auth()?.currentUser {
            
            self.performSegue(withIdentifier: "loggin", sender: nil)
        }
        ref = FIRDatabase.database().reference()
    }
    
    @IBAction func ButtonLoginFb(sender:UIButton!){
        
        
        let facebookLogin = FBSDKLoginManager()
        facebookLogin.logIn(withReadPermissions: ["email"], from: self) { (facebookResult:FBSDKLoginManagerLoginResult?, facebookError : Error?) in
            if facebookError != nil {
                print("Facebook faild to login \(facebookError) ")
                
            } else {
                
                let accessToken = FBSDKAccessToken.current().tokenString
                print("Sucessfuly loged in facebook")
                let credential = FIRFacebookAuthProvider.credential(withAccessToken: accessToken!)
                FIRAuth.auth()?.signIn(with :credential){ (user ,error) in
                    if let error = error {
                        
                        print ("errrorry\(error)")
                        
                        
                    }
                        
                    else {
                        let parameters = ["fields":"email,first_name,last_name"]
                        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (conection, results, error) in
                            if error != nil {
                            print(error)
                                return
                            } else {
                            
                            let json = JSON(results)
                                let  username = json["first_name"].string
                                self.ref.child("users").child(user!.uid).setValue(["username":username,"provider":"facebook"])
                            }
                        

                            
                        })
                        self.performSegue(withIdentifier: "loggin", sender: nil)
                        
                    }
                }
            }
        }
        
    }
    
    @IBAction func attempLogin(Sender :UIButton)
    {
        if let email = email.text, email != "" , let pwd = password.text, pwd != ""
        {
            FIRAuth.auth()?.signIn(withEmail: email, password: pwd, completion: { (FIRUser, Error) in
                if Error != nil
                {
                print(" \(Error)")
                    self.showErrorFunc(title: "Required", message: "Please put correct credential! ")
                }
                else
                {
                self.performSegue(withIdentifier: "loggin", sender: nil)
                }
            })
    }
    else
    {
    showErrorFunc(title: "Email And Password Required", message: "You must put an email and a password")
    
    }
}


func showErrorFunc(title : String , message : String)  {
    let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alert.addAction(action)
    self.present(alert, animated: true, completion: nil)
}
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    }
